<?php
session_start();


use app\controller\GroupeController;
use app\controller\LogementController;
use app\controller\UserController;
use conf\Eloquent;
use app\controller\AccueilController;

require_once 'vendor/autoload.php';

Eloquent::init('src/conf/conf.ini');

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim;

$app->get('/', function () {
    AccueilController::afficherPageAccueil();
})->name('accueil');

$app->get('/logements/', function () {
    LogementController::afficherLogements();
})->name('logements');

$app->get('/logements/:id', function ($id) {
    LogementController::afficherLogement($id);
})->name('logement');

$app->get('/users/', function () {
    UserController::afficherUsers();
})->name('users');

$app->get('/users/:id', function ($id) {
    UserController::afficherUser($id);
})->name('user');

$app->get('/connect/:id', function($id) {
    UserController::connection($id);
})->name('user_connect');

//A MODIFIER SVP
$app->get('/groupes/:id', function ($id) {
    GroupeController::afficherGroupe($id);
})->name('groupe');

$app->get('/croissant', function () {
    LogementController::afficherListeLogementCroissant();
});

$app->get('/decroissant', function () {
    LogementController::afficherListeLogementDecroissant();
});


$app->run();