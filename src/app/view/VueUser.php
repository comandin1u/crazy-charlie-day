<?php

namespace app\view;

use util\HTML;

class VueUser
{
    protected $users = array();

    function __construct($tab)
    {
        $this->users = $tab;
    }

    public function afficher()
    {
        if (count($this->users) === 1) {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlUser();
            $r .= HTML::foot();
            return $r;
        } else {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlUsers();
            $r .= HTML::foot();
            return $r;
        }
    }

    private function htmlUser()
    {
        $html = '<div class="container">
                    <section> 
                    <h2>L\'Utilisateur</h2>    
                    <table style="width:100%" class="table">
                        <tr>
                            <th><u>Id</u></th> 
                            <th><u>Photo</u></th> 
                            <th><u>Nom</u></th>
                            <th><u>Message</u></th> 
                        </tr>';

        $i = $this->users->id;
        $html .= '<tr>
                        <td>' . $i . '</td> 
                        <td>                          
                           <img src="../web/img/user/' . $i . '.jpg" alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>' . $this->users->nom . '</td>
                        <td>' . $this->users->message . '</td>
                      </tr>';

        $html .= '
                    </table>
                </section>
                </div>';

        return $html;
    }

    private function htmlUsers()
    {

        $html = '<div class="container"><section> 
                    <h2>Nos Utilisateurs</h2>    
                    <table class="table" style="width:100%">
                        <tr>
                            <th><u>Id</u></th> 
                            <th><u>Nom</u></th> 
                            <th><u>Photo</u></th> 
                            <th><u>Connexion</u></th>
                        </tr>';
        foreach ($this->users as $list) {
            $i = $list->id;
            $connect = \Slim\Slim::getInstance()->urlFor('user_connect');
            $html .= '<tr>
                           
                           
                        <td>' . $i . '</td> 
                        <td>' . $list->nom . '</td>
                        <td><a href="users/'.$i.'">                          
                           <img src="web/img/user/' . $i . '.jpg" alt="Image" style="width:304px;height:228px;">
                        </a>
                        </td>
                        <td><form action=""><button type="button" class="btn btn-success">Se connecter</button></td>
                      </tr>';
        }
        $html .= '
                    </table>
                </section>
                </div>';

        return $html;
    }
}