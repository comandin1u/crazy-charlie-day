<?php

namespace app\view;

use util\HTML;

class VueLogement
{
    protected $logements = array();

    function __construct($tab)
    {
        $this->logements = $tab;
    }

    public function afficher()
    {
        if (count($this->logements) === 1) {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlLogement();
            $r .= HTML::foot();
            return $r;
        } else {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlLogements();
            $r .= HTML::foot();
            return $r;
        }
    }

    private function htmlLogement()
    {
        $html = '<section class = "container"> 
                    <h2>Le Logement</h2>    
                    <table style="width:100%">
                        <tr>
                            <th><u>Id</u></th> 
                            <th><u>Image</u></th> 
                            <th><u>Places</u></th> 
                        </tr>';

            $i = $this->logements->id;
            $html .= '<tr>
                        <td>' . $i . '</td> 
                        <td>                          
                           <img src="../web/img/apart/' . $i . '.jpg" alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>' . $this->logements->places . '</td>
                      </tr>';

        $html .= '
                    </table>
                </section>';

        return $html;
    }

    private function htmlLogements()
    {
        $html = '<section> 
                    <h2>Nos logements</h2>    
                    <table style="width:100%">
                        <tr>
                            <th><u>Id</u></th> 
                            <th><u>Image</u></th> 
                            <th><u>Places</u>
                             </u>
                                 <a href="./croissant"> &#x25B2 <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
                                 <a href ="./decroissant"> &#x25BC <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                             </th>   
                        </tr>';
        foreach ($this->logements as $list) {
            $i = $list->id;
            $html .= '<tr>
                        <td>' . $i . '</td> 
                        <td>
                           <a href="logements/'.$i.'">
                           <img src="web/img/apart/' . $i . '.jpg" alt="Image" style="width:304px;height:228px;">
                        </a>
                        </td>
                        <td>' . $list->places . '</td>
                      </tr>';
        }
        $html .= '
                    </table>
                </section>';

        return $html;
    }

}