<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 09/02/2017
 * Time: 16:19
 */

namespace app\view;

use app\model\Groupe;
use app\controller\GroupeController;
use util\HTML;

class VueGroupe
{
    protected $groupes = array();

    function __construct($tab)
    {
        $this->groupes = $tab;
    }

    public function afficher(){
        if (count($this->groupes) === 1) {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlGroupe();
            $r .= HTML::foot();
            return $r;
        } else {
            $r = HTML::head();
            $r .= HTML::header();
            $r .= $this->htmlGroupes();
            $r .= HTML::foot();
            return $r;
        }
    }

    private function htmlGroupe(){
        $html = '<div class="container">
                    <section> 
                    <h2>L\'Utilisateur</h2>    
                    <table style="width:100%" class="table">
                        <tr>
                            <th><u>Id</u></th> 
                            <th><u>Logement</u></th> 
                            <th><u>Créateur</u></th> 
                        </tr>';

        $i = $this->groupes->id;
        $html .= '<tr>
                        <td>' . $i . '</td> 
                        <td>                          
                           <img src="../web/img/user/' . $i . '.jpg" alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>' . $this->groupes->id . '</td>
                        <td>' . $this->groupes->idLogement . '</td>
                      </tr>';

        $html .= '
                    </table>
                </section>
                </div>';

        return $html;
    }
}