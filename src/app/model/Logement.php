<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Logement extends Model{
    protected $table = 'logement';
    protected $primaryKey = 'id';
    protected  $fillable = array('id', 'places');
    public $timestamps = false;

    public function groupes(){
        return $this->hasMany('\app\models\Groupe', 'id');
    }
}