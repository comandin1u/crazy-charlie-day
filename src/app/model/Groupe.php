<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Groupe extends Model{
    protected $table = 'groupe';
    protected $primaryKey = 'id';
    protected $fillable = array('id', 'idLogement', 'idCreateur');
    public $timestamps = false;

    public function users(){
        return $this->hasMany('\app\models\User', 'id');
    }
}