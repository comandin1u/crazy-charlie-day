<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class User extends Model{
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $fillable = array('id', 'nom', 'message', 'idGroupe');
    public $timestamps = false;

    public function groupe(){
        return $this->belongsTo('\app\models\Groupe', 'id');
    }
}