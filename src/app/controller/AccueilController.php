<?php

namespace app\controller;

use app\view\VueAccueil;

class AccueilController
{
    public static function afficherPageAccueil()
    {
        $vue = new VueAccueil();
        echo $vue->afficher();
    }
}
