<?php

namespace app\controller;

use app\model\Groupe;
use app\view\VueGroupe;

if (!isset($_SESSION)) {
    session_start();
}

class GroupeController
{


    public static function ajouterMembre($idMembre)
    {
        if (!isset($_SESSION['groupe'])) {
            $_SESSION['groupe'] = array();
        }


    }

    public static function afficherGroupe($id)
    {
        $groupe = Groupe::where( 'id', '=', $id )->first() ;
        $vue = new VueGroupe($groupe);
        echo $vue->afficher();
    }
}