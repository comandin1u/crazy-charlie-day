<?php

namespace app\controller;

use app\view\VueLogement;
use app\model\Logement;

class LogementController
{
    public static function afficherLogements()
    {
        $logements = Logement::get();
        $vue = new VueLogement($logements);
        echo $vue->afficher();
    }

    public static function afficherLogement($id)
    {
        $logement = Logement::where( 'id', '=', $id )->first() ;
        $vue = new VueLogement($logement);
        echo $vue->afficher();
    }

    public static function afficherListeLogementCroissant()
    {
        $logement = Logement::orderBy('places')->get();

        $vue = new VueLogement($logement);
        echo $vue->afficher();
    }

    public static function afficherListeLogementDecroissant()
    {
        $logement = Logement::orderBy('places', 'desc')->get();

        $vue = new VueLogement($logement);
        echo $vue->afficher();
    }
}