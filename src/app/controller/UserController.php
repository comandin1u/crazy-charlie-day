<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 09/02/2017
 * Time: 14:07
 */

namespace app\controller;

use app\view\VueUser;
use app\model\User;

class UserController
{
    public static function afficherUsers()
    {
        $users = User::get();
        $vue = new VueUser($users);
        echo $vue->afficher();
    }

    public static function afficherUser($id)
    {
        $user = User::where('id', '=', $id)->first();
        $vue = new VueUser($user);
        echo $vue->afficher();
    }

    public static function connection($id) {
        session_start();
        $user = User::where('id', '=', $id)->first();
        $SESSION['username'] = $user->nom;
        $SESSION['id'] = $user->id;
    }
}