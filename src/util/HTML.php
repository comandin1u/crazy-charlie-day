<?php
namespace util;

class HTML{


	//début de la page
	public static function head(){

        $acc = \Slim\Slim::getInstance()->urlFor('accueil');
		$r=<<<END
<!DOCTYPE html>
<html lang="fr">
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Un Toit pour Tous - Page Web</title>

    <!-- Bootstrap Core CSS -->
    <link href="${acc}vendor/components/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="${acc}web/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${acc}vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="${acc}web/css/tab.css" rel="stylesheet" type="text/css" />

</head>

<body id="page-top" class="index">
END;
;
    	return $r;
	}

        public static function header(){

            $acc = \Slim\Slim::getInstance()->urlFor('accueil');
        $r=<<<END
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="${acc}">Un Toit partagé</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                      <li class="page-scroll">
                        <a href="${acc}">Accueil</a>
                    </li>
                    <li class="page-scroll">
                        <a href="${acc}logements">Logements</a>
                    </li>
                    <li class="page-scroll">
                        <a href="${acc}users">Locataires</a>
                    </li>
                    <li class="page-scroll">
                        <a href="./groupes">Groupes</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="${acc}web/img/web/banniere.jpg" alt="">
                    <div class="intro-text">
                        <span class="name">Le partage d'un toit</span>
                        <hr class="star-light">
                        <span class="skills">Association pour la mixité générationelle</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
END;

        return $r;
    }

	//fin de page
	public static function foot(){
		$r=<<<END
<footer class="text-center">
        
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Your Website 2016
                    </div>
                </div>
            </div>
        </div>
    </footer></body></html>
END;
;
		return $r;
	}
}