<?php
namespace util;

use s07\modele\Utilisateur;
use s07\modele\Role;

class Authentication{
	public static function createUser($nom, $email, $passWord1, $passWord2){
		if ($nom==""||$email==""||$passWord1==""||$passWord2=="") {
			return "Un des champs n'a pas été remplit";
		}
		if ($passWord1!=$passWord2) {
			return "Les mots de passe ne correspondent pas";
		}
		$g=utilisateur::where('email','=',$email)->first();
		if (isset($g)){
			return "L'adresse e-mail est déjà utilisée";
		}
		else{
			if (strlen($passWord1)<6) {
				return "Les mots de passe doivent au moins avoir 6 caractères.";
			}
			else{
				$g=new Utilisateur();
				$g->nom_complet=$nom;
				$g->email=$email;
				$g->password=password_hash($passWord1, PASSWORD_DEFAULT,['cost'=>12]);
				$g->roleid=1;
				$g->save();
			}
		}
	}
	public static function authenticate($email, $passWord){
		$u=utilisateur::where('email','=',$email)->first();
		if (isset($u)) {
			if (password_verify($passWord,$u->password)) {
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	public static function loadProfile($email){
		$u=utilisateur::where('email','=',$email)->first();
		$r=role::where('roleid','=',$u->roleid)->first();
		$array = array('uid'=>$u->uid,
					   'nom_complet'=>$u->nom_complet,
					   'email'=>$email,
					   'roleid'=>$r->roleid,
					   'auth_level'=>$r->auth_level);
		unset($_SESSION['profil']);
		$_SESSION['profil']=$array;
	}

	public static function checkAccessRights($required){
		if ($_SESSION['profil']['auth_level']>=$required) {
			return true;
		}
		else{
			return false;
		}
	}

	public static function disconnect(){
		if (isset($_SESSION['profil'])) {
			unset($_SESSION['profil']);	
		}
	}
}